import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class RewardedInterstitialAds extends StatefulWidget {
  const RewardedInterstitialAds({super.key});

  @override
  State<RewardedInterstitialAds> createState() =>
      _RewardedInterstitialAdsState();
}

class _RewardedInterstitialAdsState extends State<RewardedInterstitialAds> {
  bool isLoaded = false;
  RewardedInterstitialAd? rewardedInterstitialAds;

  @override
  void initState() {
    super.initState();
    loadRewardedAd();
  }

  @override
  void dispose() {
    rewardedInterstitialAds?.dispose();
    super.dispose();
  }

  void loadRewardedAd() {
    RewardedInterstitialAd.load(
        adUnitId: 'ca-app-pub-3940256099942544/5354046379',
        request: const AdRequest(),
        rewardedInterstitialAdLoadCallback: RewardedInterstitialAdLoadCallback(
            onAdLoaded: (RewardedInterstitialAd ad) {
          print('Rewarded ad loaded');

          setState(() {
            rewardedInterstitialAds = ad;
          });
        }, onAdFailedToLoad: (LoadAdError loadAdError) {
          print('Rewarded ad failed to load: $loadAdError');
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rewarded'),
      ),
      body: ListView(
        children: [
          Center(
            child: ElevatedButton(
              onPressed: () {
                rewardedInterstitialAds?.show(onUserEarnedReward: (ad, reward) {
                  print('Watched');
                });
              },
              child: const Text('Show Ads'),
            ),
          )
        ],
      ),
    );
  }
}
