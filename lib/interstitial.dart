import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class InterstialAd extends StatefulWidget {
  const InterstialAd({super.key});

  @override
  State<InterstialAd> createState() => _InterstialAdState();
}

class _InterstialAdState extends State<InterstialAd> {
  @override
  void initState() {
    super.initState();
    initInterstialAd();
  }

  late InterstitialAd interstitialAd;
  bool isAdLoaded = false;
  var adUnitId = "ca-app-pub-3940256099942544/1033173712";

  initInterstialAd() {
    InterstitialAd.load(
        adUnitId: adUnitId,
        request: const AdRequest(),
        adLoadCallback: InterstitialAdLoadCallback(onAdLoaded: (ad) {
          interstitialAd = ad;
          setState(() {
            isAdLoaded = true;
          });
          interstitialAd.fullScreenContentCallback = FullScreenContentCallback(
            onAdDismissedFullScreenContent: (ad) {
              ad.dispose();
            },
            onAdFailedToShowFullScreenContent: (ad, error) {
              ad.dispose();
            },
          );
        }, onAdFailedToLoad: (error) {
          interstitialAd.dispose();
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('InterstialAd'),
      ),
      body: ListView(
        children: [
          Center(
            child: ElevatedButton(
                onPressed: () {
                  if (isAdLoaded) {
                    interstitialAd.show();
                  } else {
                    initInterstialAd();
                  }
                },
                child: const Text('ShowAds')),
          )
        ],
      ),
    );
  }
}
