import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class RewardedAds extends StatefulWidget {
  const RewardedAds({super.key});

  @override
  State<RewardedAds> createState() => _RewardedAdsState();
}

class _RewardedAdsState extends State<RewardedAds> {
  bool isLoaded = false;
  RewardedAd? rewardedAd;

  @override
  void initState() {
    super.initState();
    loadRewardedAd();
  }

  @override
  void dispose() {
    rewardedAd?.dispose();
    super.dispose();
  }

  void loadRewardedAd() {
    RewardedAd.load(
      adUnitId: 'ca-app-pub-3940256099942544/5224354917',
      request: const AdRequest(),
      rewardedAdLoadCallback: RewardedAdLoadCallback(
        onAdLoaded: (ad) {
          print('Rewarded ad loaded');

          setState(() {
            rewardedAd = ad;
          });
        },
        onAdFailedToLoad: (error) {
          print('Rewarded ad failed to load: $error');
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rewarded'),
      ),
      body: ListView(
        children: [
          Center(
            child: ElevatedButton(
              onPressed: () {
                rewardedAd?.show(onUserEarnedReward: (ad, reward) {
                  print('Watched');
                });
              },
              child: const Text('Show Ads'),
            ),
          )
        ],
      ),
    );
  }
}
