import 'package:ad_sense/pip_view.dart';
import 'package:ad_sense/rewarded_ads.dart';
import 'package:ad_sense/rewarded_interstitial_ad.dart';
import 'package:flutter/material.dart';

import 'google_ads.dart';
import 'interstitial.dart';

class AdsButton extends StatefulWidget {
  const AdsButton({super.key});

  @override
  State<AdsButton> createState() => _AdsButtonState();
}

class _AdsButtonState extends State<AdsButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Center(child: Text("Google Ads"))),
      body: Column(
        children: [
          const SizedBox(height: 20),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const GoogleAds()));
                },
                child: const Text("BannerAds")),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const InterstialAd()));
              },
              child: const Text("InterstialAds")),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const RewardedAds()));
              },
              child: const Text("RewardedAds")),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const RewardedInterstitialAds()));
              },
              child: const Text("RewardedInterstitialAds")),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const PipView()));
              },
              child: const Text("PipView")),
        ],
      ),
    );
  }
}
